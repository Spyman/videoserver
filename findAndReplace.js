const fs = require('fs');

var root = "Storage"; 
var divider = "/"; 
var p = fs.readdirSync(root);

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

function findSumm(list) {
	var lIndexes = []; 
	var rIndexes = []; 
	for (var i = 1; i < list.length; i++) {
		var currName = list[i].replaceAll("_"," ").replaceAll("."," ").replaceAll("  ", " "); 
		var previousName = list[i - 1].replaceAll("_"," ").replaceAll("."," ").replaceAll("  ", " "); 
		var leftIndex = 0; 
		for (var fc = 0; fc < currName.length; fc++) {
			if (currName[fc] == previousName[fc]) {
				leftIndex = fc + 1; 
			} else {
				lIndexes[i-1] = leftIndex; 
				break; 
			}
		}
		var rightIndex = 0; 
		for (var fc = 0; fc < currName.length; fc++) {
			if (currName[currName.length - fc - 1] == previousName[previousName.length - fc - 1]) {
				rightIndex = fc; 
			} else {
				rIndexes[i-1] = rightIndex; 
				break; 
			}
		}
	}
	
	var lmin = lIndexes[0]; 
	for (var i = 0; i < lIndexes.length; i++) {
		if (lIndexes[i] < lmin) {
			lmin = lIndexes[i]; 
		}
	}
	

	var rmin = rIndexes[0]; 
	for (var i = 0; i < rIndexes.length; i++) {
		if (rIndexes[i] < rmin) {
			rmin = rIndexes[i]; 
		}
	}
	
	for (var i = 0; i < list.length; i++) { 
		var newName = list[i].substring(lmin, list[i].length - rmin - 1).replaceAll("_"," ").replaceAll("."," ").replaceAll("  ", " ");
		if (newName.length < 5) {
			newName = "Episode " + newName;
		}
		
		var fullSerialPath = root + divider + p[j] + divider + s[n] + divider + list[i];  
		var fullNewPath = root + divider + p[j] + divider + s[n] + divider + newName + ".mp4"; 
		
		if (fullNewPath != fullSerialPath) {
			console.log(fullSerialPath);
			fs.rename(fullSerialPath, fullNewPath);  
		}
	}
}

for (var j = 0; j < p.length; j++) {
	try {
		var s = fs.readdirSync(root + divider + p[j]); 
		for (var n = 0; n < s.length; n++) {
			try {
				var season = fs.readdirSync(root + divider + p[j] + divider + s[n]);
				findSumm(season); 
			} catch(ex) {
			
			}
		}
	} catch (ex) {
		
	}
}


	
