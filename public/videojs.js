var vview; 

document.addEventListener("DOMContentLoaded", function(event) { 
	vview = document.getElementById('video');
	var vol = localStorage.getItem('volume') 
	if (vol != undefined) {
		vview.volume = vol; 
	}

	vview.addEventListener("volumechange", function() {
		localStorage.setItem('volume', vview.volume); 
	});
});

var moveToNextVideo = function() {
	var info = getInfoFromUrl(); 
	var nextInfo = findNextInfo(map, info.serial, info.season, info.seria);
	if (nextInfo == undefined) {
		document.location.href = "/watch/" + info.serial + "/" + info.season + "/undefined"; return; 
	}; 
	var nextVideoPath = "/video/" + nextInfo.serial + "/" + nextInfo.season + "/" + nextInfo.seria; 
	var nextUrlPath = "/watch/" + nextInfo.serial + "/" + nextInfo.season + "/" + nextInfo.seria;
	
	document.location.href = nextUrlPath; 
} 

var moveToSerial = function() {
	var info = getInfoFromUrl(); 
	var serialPath = "/folder/" + info.serial 
	document.location.href = serialPath; 
}

var moveToSeason = function() {
	var info = getInfoFromUrl(); 
	var seasonPath = "/serial/" + info.serial + "/" + info.season; 
	document.location.href = seasonPath; 
}

function getTimeCode() {
	var link = window.location.href
	var position = link.lastIndexOf('#')
	if (position == -1) {
		return "0"
	}
	return link.substr(position, link.length)
}

function savePage() {
	var http = new XMLHttpRequest();
	var url = '/save';
	var info = getInfoFromUrl()
	var params = '{"name":"' + info.serial + ' | ' + info.season + ' | ' + info.seria + ' |' + getTimeCode()  + '", "value":"' + window.location.href + '"}';
	http.open('POST', url, true);

	//Send the proper header information along with the request
	http.setRequestHeader('Content-type', 'application/json');

	http.onreadystatechange = function() {//Call a function when the state changes.
		if(http.readyState == 4 && http.status == 200) {
			
		}
	}
	http.send(params);
}

function videoNotInFocus() {
	return document.activeElement != vview; 	
}

function ppause() {
	if (videoNotInFocus()) {
		return 
	}
	
	if (vview.paused) {
		vview.play() 
	} else {
		vview.pause() 
	}
}

function pleft() {
	if (videoNotInFocus()) {
		return 
	}
	
	vview.pause()
	vview.currentTime -= 15
	vview.play() 
}

function pright() {
	if (videoNotInFocus()) {
		return 
	}
	
	vview.pause()
	vview.currentTime += 15
	vview.play() 
}