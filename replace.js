const fs = require('fs');

var root = "Storage"; 
var divider = "/"; 
var p = fs.readdirSync(root);

function replaceExtention(ext) {
	var exts = ["mp4", "avi", "mkv", "ts"];
	
	var list = ext.split('.'); 
	for (var i = list.length - 1; i > -1; i--) {
		for (var j = 0; j < exts.length; j++) {
			if (list[i] == exts[j]) {
				list[i] = ""; 
			}
		}			
	}
	
	var result = ""; 
	for (var i = 0; i < list.length; i++) {
		if (list[i] != "") {
			result += list[i] + "_"; 
		}
	}
	
	return result.substring(0, result.length - 1) + ".mp4"; 
}

for (var j = 0; j < p.length; j++) {
	try {
		var s = fs.readdirSync(root + divider + p[j]); 
		for (var n = 0; n < s.length; n++) {
			try {
				var season = fs.readdirSync(root + divider + p[j] + divider + s[n]);
				for (var i = 0; i < season.length; i++) {
					var seria = season[i]; 
					var fullSerialPath = root + divider + p[j] + divider + s[n] + divider + seria; 
					var result = replaceExtention(seria); 
					var fullNewPath = root + divider + p[j] + divider + s[n] + divider + result; 
					
					if (fullNewPath != fullSerialPath) {
						console.log(fullSerialPath);
						fs.rename(fullSerialPath, fullNewPath);  
					}
				}
			} catch(ex) {}
		}
	} catch (ex) {}
}


	
