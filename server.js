﻿const express = require('express');
const fs = require('fs');
const path = require('path');

const ffmpeg = require('fluent-ffmpeg');
const command = ffmpeg();

const app = express();
const two_week = 1209600000; 

const bodyParser = require('body-parser');

app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));

Number.prototype.pad = function (size) {
    var s = String(this);
    while (s.length < (size || 2)) {
        s = "0" + s;
    }
    return s;
}

snow = "<div id='back'></div>"
let cssBaseImport = "" 
let cssMainImport = "" 

var videoHeader = '<html><head><script src="/videojs.js"></script><link href="/main.css" rel="stylesheet" type="text/css"><link rel="shortcut icon" href="/favicon.png" type="image/png"><link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"><meta name="robots" content="none"/><meta charset="utf-8"><title>Big LIB</title></head><body style="margin:0px; background: #000;">';

var fotter = '</div>' + snow + '</body></html>';
var videofotter = "<div style='display:flex; justify-content: center; align-items: center;'><div style='display: flex; flex-grow: 1;width: 33%; justify-content: center;align-items: center;'><a style='color:#fff; cursor:pointer; font-weight:600; font-size:22px;' align='center' onclick='moveToSerial()'>К сериалу</a></div><div style='display: flex; flex-grow: 1;width: 33%; justify-content: center;align-items: center;'><a style='color:#fff; cursor:pointer; font-weight:600; font-size:22px;' align='center' onclick='moveToSeason()'>К сезону</a></div><div style='display: flex; flex-grow: 1;width: 33%; justify-content: center;align-items: center;'><a style='color:#fff; cursor:pointer; font-weight:600; font-size:22px;' align='center' onclick='savePage()'>Закладка</a></div><div style='display: flex; flex-grow: 1;width: 33%; justify-content: center;align-items: center;'><a style='color:#fff; cursor:pointer; font-weight:600; font-size:22px;' align='center' onclick='moveToNextVideo()'>Следующая</a></div></div>" + fotter

var root = '/';
var divider = '/';
var rootDir = 'Storage/';
var serialsMap = {};
var updated = {}

var seriesCount = 0;
var startTime = new Date().toLocaleString();

let filmfolderMark = '.filmfolder'; 
var films = []
var nowWatch = {}; 

app.use('/favicon.png', express.static('public/favicon.png'));

function getFileExt(filename) {
    //return filename.split('.').pop();
    return 'mp4';
}

function fillHeader(addParams) {
	let firstPart = '<html><head><meta name="robots" content="none"/><meta charset="utf-8">' 
	let defaultJs = '<script src="/js.js"></script>'
	let mainCss = '<link href="/main.css" rel="stylesheet" type="text/css">'
	let defaultTitle = '<title>Big LIB</title>'
	let favicon = '<link rel="shortcut icon" href="/favicon.png" type="image/png">'
	let bootstrap = '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">'
	let ender = '</head><body><div class="container gradient opacity">'
	
	var center = "" 
	if (addParams != undefined) {
		for (var i = 0; i < addParams.length; i++) {
			center += addParams[i]
		}
	}
	return firstPart + defaultJs + mainCss + defaultTitle + favicon + bootstrap + center + ender
}

function fillDefaultHeader() {
	return fillHeader([createStylesheet("gback.css"), createStylesheet("glist.css")])
}

function createStylesheet(filename) {
	return '<link rel="stylesheet" href="/' + filename + '" type="text/css">'; 
}

function onFlyCompress(stream, dimention) {
    return ffmpeg(stream)
        .videoCodec('libvpx')
        .size(dimention)
        .format('webm')
        .on('end', function () {
            console.log('Processing finished !');
        })
        .on('progress', function (progress) {
            console.log('Processing: ' + progress.percent + '% done');
        })
}

function checkUpdateTime(fullpath, serial) {
    var stats = fs.statSync(fullpath);
    var mtime = stats.mtime;
    var last = updated[serial];

    if (last == undefined) {
        updated[serial] = {date: new Date(mtime), maxDate: new Date(mtime)}
        return;
    }

    if (mtime > last.date) {
        updated[serial] = {date: new Date(mtime), maxDate: last.maxDate}
    }

    if (mtime <= last.maxDate) {
        updated[serial] = {date: last.date, maxDate: mtime}
    }
}


function createFileTree() {
    console.log("read start");
	serialsMap = {};
    var serials = fs.readdirSync(rootDir);
	films = []
    for (var i = 0; i < serials.length; i++) {
        try {
            var serial = serials[i];
            var seasonsFull = fs.readdirSync(rootDir + serial);

            var seasons = [];
            for (var n = 0; n < seasonsFull.length; n++) {
                var str = seasonsFull[n].split(".");
                var ext = str[str.length - 1];
                if (ext != "torrent") {
                    seasons.push(seasonsFull[n]);
                }
            }

            var seasonsMap = {};
            for (var j = 0; j < seasons.length; j++) {
                try {
                    var season = seasons[j];

					let seriesPath = rootDir + serial + divider + season
                    var series = fs.readdirSync(seriesPath)
					let isFilmFolder = fs.existsSync(seriesPath + divider + filmfolderMark)
					
                    seasonsMap[season] = [];
                    for (var n = 0; n < series.length; n++) {
                        var spl = series[n].split(".");
                        if (spl[spl.length - 1] == "mp4") {
                            seasonsMap[season].push(series[n]);
                            if (isFilmFolder) {
								films.push({
									name: series[n],
									serial: serials[i],
									season: season, 
								}); 
							}
							seriesCount++;
                        }
                        checkUpdateTime(rootDir + serial + divider + season + divider + series[n], serial);
                    }
                } catch (ex) {
                    console.log(ex);
                }
            }

            serialsMap[serial] = seasonsMap;
        } catch (exception) {
        }
    }
    console.log("ready");
}

createFileTree();

function mapNameBage(name, bage) {
	let key = "@@@"
	let numkey = "№№№"
	if (name.includes(numkey)) {
		name = name.split(numkey)[1]
	}
	if (!name.includes(key)) {
		return name 
	}
	let split = name.split(key)
	let ru = split[0]
	let en = split[1]
	return "<span style='white-space: nowrap ;'><text>" + ru + "</text><text> </text>" + bage + "<span style='color: transparent; text-overflow: clip;'>" + en + "</span></span>"
}

function mapName(name) {
	return mapNameBage(name, "")
}

app.get('/', function (req, res) {
    res.write(fillDefaultHeader());

    var count = Object.keys(serialsMap).length;
    res.write('<h3 id="info" style="color:#8e1c1c; transform: rotate(-90deg); transform-origin: right bottom; position:absolute; right:15; top:120;">' + startTime + ' Серий ' + seriesCount + ' Сериалов ' + count + '</h3>');
    res.write('<div><div>');
    var items = Object.keys(serialsMap);
	res.write('<div style="display:flex; flex-direction:row; justify-content: space-between;"><span>Нажми Ctrl+F для поиска</span><a href="/filmlibrary" style="color:#fff; text-decoration:none; font-size:16px; font-weight: 400">Нажми сюда для просмотра фильмотеки</a></div>')
    res.write('<div style="display:flex; flex-direction:row; justify-content: space-between;"><h1 style="color:white; display:span;">Коллекция</h1><a href="/save/history" style="padding-top:8px; color:white; text-decoration:none;">Закладки</a></div><br>');
    current = new Date();
    for (var i = 0; i < items.length; i++) {
        if (updated[items[i]] == undefined || current - updated[items[i]].date >= two_week) {
            res.write('<a href="folder/' + items[i] + '">' + mapName(items[i]) + '</a><br>');
        } else {
            d = updated[items[i]].date;
            var curr_date = d.getDate().pad();
            var curr_month = (d.getMonth() + 1).pad();
            var curr_year = d.getFullYear();
            var updatedAt = curr_date + "." + curr_month + "." + curr_year

            var bage;
            if (updated[items[i]].date - updated[items[i]].maxDate > 24 * 60 * 60 * 1000) {
                bage = '<span style="display:inline-block; margin-bottom:0px; padding-left:14px; padding-right:14px;"><span title="Последнее обновление ' + updatedAt + '" class="badge badge-primary">Updated </span></span>';
            } else {
                bage = '<span style="display:inline-block; margin-bottom:0px; padding-left:14px; padding-right:14px;"><span title="Добавлено ' + updatedAt + '" class="badge badge-danger">New </span></span>';
            }
            res.write('<a href="folder/' + items[i] + '">' + mapNameBage(items[i], bage) + '</a><br>');
        }
    }

    res.write('</div></div></div>');
    res.write(fotter);
    res.end();
})

app.get('/folder/:serial', function (req, res) {
    var items = Object.keys(serialsMap[req.params.serial]);

    if (items.length == 1) {
        req.params.seazon = items[0];
        onSeasonOpen(req, res);
        return;
    }

    res.setHeader('content-type', 'text/html; charset=utf-8');
    res.write(fillDefaultHeader());

    var filePath = rootDir + req.params.serial + divider + "tfile.torrent";
    var exists = fs.existsSync(filePath);

    if (exists) {
        res.write('<div class="row"><div class="col-sm"><h1 style="color:white;">' + req.params.serial + '</h1></div><div class="col-sm" align="right"><a href="/download/' + req.params.serial + '/tfile.torrent">Скачать</a></div></div>');
    } else {
        res.write('<h1 style="color:white;">' + mapName(req.params.serial) + '</h1>');
    }

    if (items.length < 1) {
        res.write('</br></br><h1 style="color:white;">Cooming soon</h1>');
        res.write('<h3 style="color:white;">Decoding now</h3><br>');
    }

    for (var i = 0; i < items.length; i++) {
        res.write('<a href="/serial/' + req.params.serial + divider + items[i] + '">' + items[i] + '</a><br>');
    }

    res.write(fotter);
    res.end();
})

app.get('/download/:serail', function (req, res) {
    res.download(rootDir + req.params.serail + ".torrent");
});

var onSeasonOpen = function (req, res) {
    res.setHeader('content-type', 'text/html; charset=utf-8');
    res.write(fillDefaultHeader());

    var filePath = rootDir + req.params.serial + divider + "tfile.torrent";
    var exists = fs.existsSync(filePath);
    
    if (exists) {
        res.write('<div class="row"><div class="col-sm"><h1 style="color:white;">' + mapName(req.params.serial) + '</h1></div><div class="col-sm" align="right"><a href="/download/' + req.params.serial + '/tfile.torrent">Скачать</a></div></div>');
    } else {
        res.write('<h1 style="color:white;">' + mapName(req.params.serial) + '</h1>');
    }

    res.write('<h2 style="color:white;">' + req.params.seazon + '</h2>');

    var items = Object.values(serialsMap[req.params.serial][req.params.seazon]);
    if (items.length < 1) {
        res.write('<h1>Cooming soon</h1></br>');
        res.write('<h3>Decoding now</h3><br>');
    }

    for (var i = 0; i < items.length; i++) {
        res.write('<a href="' + root + 'watch/' + req.params.serial + divider + req.params.seazon + divider + items[i] + '">' + mapName(items[i].substring(0, items[i].length - 4)) + '</a><br>');
    }

    res.write(fotter);
    res.end();
};

app.get('/serial/:serial/:seazon', onSeasonOpen)

app.get('/filmlibrary', function (req, res) {
	
	res.setHeader('content-type', 'text/html; charset=utf-8');
    res.write(fillHeader([createStylesheet("fback.css"), createStylesheet("glist.css")]));

	res.write('<div style="display:flex; flex-direction:row; justify-content: space-between;"><span style="color:#fff;">Нажми Ctrl+F для поиска</span></div>')
    res.write('<h2 style="color:white;">Фильмотека</h2><br>');

    var items = films; 
    var prev = "###" 
	for (var i = 0; i < items.length; i++) {
		let film = items[i]
		if (prev != film.serial) {
			res.write('<h3 style="color:#999; margin-top:8px; margin-botton:16px;">' + mapName(film.serial) + '</h3>')
			prev = film.serial
		}
        res.write('<a href="' + root + 'watch/' + film.serial + divider + film.season + divider + film.name + '">' + mapName(film.name.substring(0, film.name.length - 4)) + '</a><br>');
    }

    res.write(fotter);
    res.end();
})

app.get('/watch/:serial/:seazon/:video', function (req, res) {
    res.setHeader('content-type', 'text/html; charset=utf-8');
    var ext = getFileExt(req.params.video);

    var path = root + 'video' + divider + req.params.serial + divider + req.params.seazon + divider + req.params.video;
    var name = (req.params.serial + " " + req.params.seazon + " " + req.params.video).replace("'", "").replace('"', "");


    if (req.params.video != "undefined") {

        res.write(videoHeader);
        res.write('<video playsinline autoplay="autoplay" style="height: 100%; width: 100%; outline: none;" id="video" controls preload="auto"><source id="source" src="' + path + '" type="video/' + ext + '"></video><br>');
        res.write('<script type="text/javascript">var nv = document.getElementById("video"); var rurl = window.location.href; var nurl = rurl.substring(0, rurl.indexOf("#")); if (rurl.indexOf("#") != -1) { var time = rurl.substring(rurl.indexOf("#") + 1, rurl.length); if (time != "") {nv.oncanplay = function() {nv.currentTime = time; nv.oncanplay = null; } }} var map = ' + JSON.stringify(serialsMap[req.params.serial]) + '; function findNextInfo(map, serial, season, seria) { var seasons = Object.keys(map); var series = map[season]; var position = series.indexOf(seria); if (position < series.length - 1) {return { serial: serial, season: season, seria: series[position + 1] }; } position = seasons.indexOf(season); if (position < seasons.length - 1) {return { serial: serial, season: seasons[position + 1], seria: map[seasons[position + 1]][0] };} return undefined; }; function getInfoFromUrl() {var url = window.location.href; var urlmap = url.split("/"); var seria = decodeURI(urlmap[urlmap.length - 1]); var season = decodeURI(urlmap[urlmap.length - 2]); var serial = decodeURI(urlmap[urlmap.length - 3]); if (seria.indexOf("#") != -1) {seria = seria.substring(0, seria.indexOf("#"));} return {serial: serial, season: season, seria: seria} }; var findNext = function(e) {var info = getInfoFromUrl(); var nextInfo = findNextInfo(map, info.serial, info.season, info.seria); if (nextInfo == undefined) {document.location.href = "/watch/" + info.serial + "/" + info.season + "/undefined"; return; }; var nextVideoPath = "/video/" + nextInfo.serial + "/" + nextInfo.season + "/" + nextInfo.seria; var nextUrlPath = "/watch/" + nextInfo.serial + "/" + nextInfo.season + "/" + nextInfo.seria; document.getElementById("source").setAttribute("src", nextVideoPath); var videoView = document.getElementById("video"); videoView.load(); videoView.play(); nurl = nextUrlPath; window.history.replaceState({page: "' + name + '"}, "' + name + '", nextUrlPath); }; document.getElementById("video").addEventListener("ended", findNext); nv.ontimeupdate = (event) => {window.history.replaceState({page: "' + name + '"}, "' + name + '", nurl + "#" + video.currentTime);}; </script>');
        res.write(videofotter);
    } else {
        res.write(fillDefaultHeader());
        res.write("<h1 style='color:white;'>Вот и все, ребята</h1>");
        res.write("<a href='/'>А что мне делать дальше?</a>");
        res.write(fotter);
    }

    res.end();
})

app.get('/hotswap', function (req, res) {
    seriesCount = 0;
    createFileTree();
    res.setHeader('content-type', 'text/html; charset=utf-8');
    res.write(fillDefaultHeader());
    res.write("<h1 style='color:white;'>HOT SWAP COMPLETE SUCCESSFULL</h>");
    res.write(fotter);
    res.end();

    startTime = startTime = new Date().toLocaleString();
});


app.get('/cvideo/:serial/:seazon/:file/:dimention', function (req, res) {
    console.log("load " + req.params.serial + " " + req.params.seazon + " " + req.params.file + " " + req.ip);

    const path = rootDir + req.params.serial + divider + req.params.seazon + divider + req.params.file;
    const stat = fs.statSync(path)
    const fileSize = stat.size
    const range = req.headers.range
    const dimention = req.params.dimention;
    var ext = "webm";


    if (range) {
        const parts = range.replace(/bytes=/, "").split("-")
        const start = parseInt(parts[0], 10)

        const end = parts[1]
            ? parseInt(parts[1], 10)
            : fileSize - 1

        const chunksize = (end - start) + 1
        const head = {
            'Content-Range': `bytes ${start}-${end}/${fileSize}`,
            'Accept-Ranges': 'bytes',
            'Content-Length': chunksize,
            'Content-Type': 'video/' + ext,
        }

        res.writeHead(206, head)
        onFlyCompress(fs.createReadStream(path, {start, end}), dimention).pipe(res)
    } else {
        const head = {
            'Content-Length': fileSize,
            'Content-Type': 'video/' + ext,
        }
        res.writeHead(200, head)
        onFlyCompress(fs.createReadStream(path), dimention).pipe(res)
    }
})

app.get('/video/:serial/:seazon/:file', function (req, res) {	
	nowWatch[req.ip] = {
		ip: req.ip, 
		serial: req.params.serial,
		season: req.params.seazon, 
		video: req.params.file, 
		time: new Date().getTime()
	}

    const path = rootDir + req.params.serial + divider + req.params.seazon + divider + req.params.file;
    const stat = fs.statSync(path)
    const fileSize = stat.size
    const range = req.headers.range
    var ext = getFileExt(req.params.file);


    if (range) {
        const parts = range.replace(/bytes=/, "").split("-")
        const start = parseInt(parts[0], 10)

        const end = parts[1]
            ? parseInt(parts[1], 10)
            : fileSize - 1

        const chunksize = (end - start) + 1
        const file = fs.createReadStream(path, {start, end})
        const head = {
            'Content-Range': `bytes ${start}-${end}/${fileSize}`,

            'Content-Length': chunksize,
            'Content-Type': 'video/' + ext,
        }

        res.writeHead(206, head)
        file.pipe(res)
    } else {
        const head = {
            'Content-Length': fileSize,
            'Content-Type': 'video/' + ext,
        }
        res.writeHead(200, head)
        fs.createReadStream(path).pipe(res)
    }
})


app.post('/api/all', function (req, res) {
    res.setHeader('content-type', 'text/json; charset=utf-8');
    res.write(JSON.stringify(serialsMap));
    res.end();
})

app.post('/api/serial', function (req, res) {
    res.setHeader('content-type', 'text/json; charset=utf-8');
    res.write(JSON.stringify(Object.keys(serialsMap)));
    res.end();
})

app.post('/api/serial/:serial', function (req, res) {
    res.setHeader('content-type', 'text/json; charset=utf-8');
    res.write(JSON.stringify(Object.keys(serialsMap[req.params.serial])));
    res.end();
})

app.post('/api/serial/:serial/:season', function (req, res) {
    res.setHeader('content-type', 'text/json; charset=utf-8');
    res.write(JSON.stringify(serialsMap[req.params.serial][req.params.season]));
    res.end();
})

app.get('/status', function(req, res, next) {
	res.send(nowWatch); 
});


var storageModule = require('./modules/storage'); 
app.use('/save', storageModule); 

app.listen(3001, function () {
})